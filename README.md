> - **DEPRECATED: please refer to the [Orchidea](https://forum.ircam.fr/projects/detail/orchidea/) project.**
>
> - **WARNING: Orchids is not compatible with macOS High Sierra and Catalina.**

Orchids is the first complete system for abstract and temporal computer-assisted orchestration and timbral mixture optimization. It provides a set of algorithms and features to reconstruct any time-evolving target sound with a combination of acoustic instruments, given a set of psychoacoustic criteria. It can help composers to achieve unthinkable timbral colors by providing efficient sets of solutions that best match a sound target. With our extended set of features, it can now also reproduce abstract spectral descriptors movements (by passing the need for a sound file) by combinations of any sounds. Its results provide multiple orchestral scores that can be arranged in a timeline in order to perform a fast sketching of musical ideas. The new system provides several algorithms for approximating jointly several timbre properties. The advantages of the Orchids system is that this approximation can be made separately over temporal shapes, mean values or standard deviations (or any combinations of the three) of each psychoacoustic descriptor. Furthermore, users can also defines a manual temporal warping, and even perform multiple search inside parts of a sound, quickly providing full orchestral pieces in seconds. The new system provides an extensive out-of-the-box database of orchestral possibilities but can also be unlimitedly extended, even with synthesis sounds, by a simple drag-and-drop operation. Finally, the software provides an intelligent time series querying system that allow to easily search for temporal shapes inside the sound database.

## Features ##

- **From audio to multiple abstract targets.** The system allows using a single sound target or mix the features of multiple targets. Then, modifying any features shapes and values and mix it with original features,  or defining multiple search problems to be handled separately inside a single target. Finally, the user can start from a void set (abstract target mode) and directly draw shapes to optimize (without the need to provide a sound file)

- **Precise psychoacoustic features.** A complete and extensible set of over 40 psychoacoustic descriptors automatically computed for database and target sounds.
- **Flexible constraints.** Precise filters and constraints over the search space allow controls on both symbolic, spectral and temporal ways.
- **Orchestras at your fingertips.** A complete view on the classical and contemporary orchestra and much more.

    - By relying on an extensive database, Orchids provides out-of-the-box the temporal analysis of psychoacoustic descriptions from over 30 orchestral instruments and 600 playing styles providing a complete and subtle description of orchestral capabilities

    - Now embeds a spatialization system to pre-arrange your orchestral space and be able to obtain a higher fidelity in the preview of future orchestral renderings
			
- **Algorithmic warfare.** Our new C++ implementation extends state-of-art algorithms by providing fast and efficient solutions in seconds, even to long temporal problems featuring wide orchestral staff.
	
    - *Static optimization.* Provides fast solutions for timbral mixture colors
    
    - *Optimal warping.* Defined specifically for temporal problems, this algorithm provides complex and diverse solutions by combining multi-objective and time series matching algorithms.
    
    - *Manual warping.* Provides a fine-grain and user-defined control over the temporality of targets.
    
    - *Multi-target.* Defines a search for an articulated long orchestral score defined through the continuity over multiple pieces of a target.
	
- **Intelligent sound samples database.** The new database allows to perform queries for sound files based on the temporal evolution of their spectral descriptors. Therefore, when searching for specific properties of a sound, it is possible to directly draw the desired temporal shapes. Furthermore, the efficient implementation provides an unlimited extensibility by simple drag and drop operations.
- **Populations of multi-dimensional results** provide a new window on sound imaginary by allowing to explore multimodal search spaces. Parse through solutions and see spectral features and optimization distances, while the right part allows to see the symbolic score for each solution.
- **Intuitive and fast sketching.** The novel timeline module provides an automatic and intuitive handling of orchestral macro-articulations.
- **Exports and settings.** The extensive parameter settings and multiple output formats provide full control over the creative workflow.

## Fields of applications ##

- **Composition.** Accessing unheard orchestral mixtures and unprecedented combinations. Possibility to obtain a symbolic score linking the spectral world of acoustic properties.
- **Pedagogy.** Unique tool to exemplify the richness, diversity and almost infinite palette of orchestral mixtures in orchestration school classes.
- **Sound design and synthesis.** Unlimited possibilities of timbre mixtures, even with synthesis sounds by optimizing multiple psychoacoustic descriptors simultaneously.
- **Computer music.** Programmatically control sound combinations writing and timelines through spectral descriptors.
- **Cinema & Video.** Intuitive and straightforward generation of orchestral pieces.
- **Post-production.** Fast process for re-orchestrating existing musical pieces.
- **Scientific Research & Development.** Most efficient tool to re-generate any evolutions of multiple psychoacoustic descriptors re-created by sound combinations.
- **Amateur.** Quickly and intuitively access unlimited possibilities of orchestral writing or any type of timbre combination synthesis

> - Designed and developed by the [Musical Representations](https://www.ircam.fr/recherche/equipes-recherche/repmus/) Team.
> 
> - [Computer-Assisted Composition (Orchids)](https://www.ircam.fr/projects/pages/orchestration-assistee-par-ordinateur-orchids/)
